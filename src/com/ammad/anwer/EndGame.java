package com.ammad.anwer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EndGame extends Activity implements View.OnClickListener {
	String getScore;
	TextView tvScore;
	String GameLevel;
	Button btAgain;
	Button btRate;
	String DbNameResult;
	String DbScoreResult;
	String myName;
	TextView tvName;
	TextView tScore;
	TableDB check;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.over);
		initialize();
		// Bundle getBasket = getIntent().getExtras();
		Intent i = getIntent();
		getScore = i.getStringExtra("key");
		GameLevel = i.getStringExtra("level");
		tvScore.setText("Your memory span is " + getScore);
		checkIfTableNotEmpty();
	}

	private void checkIfTableNotEmpty() {
		// TODO Auto-generated method stub

		if ((GameLevel.equals("1"))
				&& (check.getNumOfEasyRows() < 5 || Integer.parseInt(getScore) > Integer
						.parseInt(check.getFifthEasyScore()))) {
			alert();
		}

		else if ((GameLevel.equals("2"))
				&& (check.getNumOfModerateRows() < 5 || Integer
						.parseInt(getScore) > Integer.parseInt(check
						.getFifthModerateScore()))) {
			alert();
		} else if ((GameLevel.equals("3"))
				&& (check.getNumOfHardRows() < 5 || Integer.parseInt(getScore) > Integer
						.parseInt(check.getFifthHardScore()))) {
			alert();
		} else if ((GameLevel.equals("4"))
				&& (check.getNumOfVeryHardRows() < 5 || Integer
						.parseInt(getScore) > Integer.parseInt(check
						.getFifthVeryHardScore()))) {
			alert();
		} else {
			showTable();
			check.close();
		}

	}

	private void showTable() {
		// TODO Auto-generated method stub
		if (GameLevel.equals("1")) {
			DbNameResult = check.getEasyNameResult();
			DbScoreResult = check.getEasyScoreResult();
		} else if (GameLevel.equals("2")) {
			DbNameResult = check.getModerateNameResult();
			DbScoreResult = check.getModerateScoreResult();
		} else if (GameLevel.equals("3")) {
			DbNameResult = check.getHardNameResult();
			DbScoreResult = check.getHardScoreResult();
		} else if (GameLevel.equals("4")) {
			DbNameResult = check.getVeryHardNameResult();
			DbScoreResult = check.getVeryHardScoreResult();
		}
		tvName.setText(DbNameResult);
		tScore.setText(DbScoreResult);
	}

	private void alert() {
		// TODO Auto-generated method stub
		final AlertDialog AD = new AlertDialog.Builder(EndGame.this).create();
		AD.setTitle("Wohoo!!! A New Record!!!");
		final EditText etName = new EditText(this);
		etName.setHint("Enter Your Name Here");
		AD.setView(etName);
		AD.setButton("OK", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				myName = etName.getText().toString();
				if (GameLevel.equals("1"))
					check.insertEasyTable(myName, getScore);
				else if (GameLevel.equals("2"))
					check.insertModerateTable(myName, getScore);
				else if (GameLevel.equals("3"))
					check.insertHardTable(myName, getScore);
				else if (GameLevel.equals("4"))
					check.insertVeryHardTable(myName, getScore);
				showTable();
				AD.dismiss();
				check.close();
			}
		});
		AD.show();

	}

	private void initialize() {
		// TODO Auto-generated method stub
		tvScore = (TextView) findViewById(R.id.TvShowScore);
		tvName = (TextView) findViewById(R.id.tvNameData);
		tScore = (TextView) findViewById(R.id.tvScoreData);
		btAgain = (Button) findViewById(R.id.btPlayAgain);
		btAgain.setOnClickListener(this);
		btAgain.getBackground().setAlpha(150);
		btRate = (Button) findViewById(R.id.bRateThisApp);
		btRate.setOnClickListener(this);
	
		check = new TableDB(EndGame.this);
		check.open();
	}

	public void onClick(View arg0) {
		// TODO Auto-generated method stub

		switch (arg0.getId()) {
		case R.id.btPlayAgain:
			Bundle basket = new Bundle();
			basket.putString("key", GameLevel);
			Intent intent = new Intent(EndGame.this, Memory.class);
			intent.putExtras(basket);
			startActivity(intent);
			break;
		case R.id.bRateThisApp:
			
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.ammad.anwer&feature=search_result&fb_source=message#?t=W251bGwsMSwxLDEsImNvbS5hbW1hZC5hbndlciJd"));
			startActivity(browserIntent);

			break;
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

}
