package com.ammad.anwer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TableDB {

	public static final String E_NAME = "persons_name";
	public static final String E_SCORE = "persons_score";
	public static final String M_NAME = "persons_name";
	public static final String M_SCORE = "persons_score";
	public static final String H_NAME = "persons_name";
	public static final String H_SCORE = "persons_score";
	public static final String VH_NAME = "persons_name";
	public static final String VH_SCORE = "persons_score";

	private static final String DATABASE_NAME = "OurDatabase";
	private static final String DATABASE_EASY_TABLE = "EasyTable";
	private static final String DATABASE_MODERATE_TABLE = "ModerateTable";
	private static final String DATABASE_HARD_TABLE = "HardTable";
	private static final String DATABASE_VERYHARD_TABLE = "VeryHardTable";
	private static final int DATABASE_VERSION = 2;

	private DbHelper OurHelper;
	private final Context ourContext;
	private SQLiteDatabase ourDatabase;

	private static class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub

		}

		@Override
		public void onCreate(SQLiteDatabase arg0) {
			// TODO Auto-generated method stub
			arg0.execSQL("CREATE TABLE " + DATABASE_EASY_TABLE + " (" + E_NAME
					+ " TEXT, " + E_SCORE + " INTEGER);"

			);
			arg0.execSQL("CREATE TABLE " + DATABASE_MODERATE_TABLE + " ("
					+ M_NAME + " TEXT, " + M_SCORE + " INTEGER);"

			);
			arg0.execSQL("CREATE TABLE " + DATABASE_HARD_TABLE + " (" + H_NAME
					+ " TEXT, " + H_SCORE + " INTEGER);");
			arg0.execSQL("CREATE TABLE " + DATABASE_VERYHARD_TABLE + " ("
					+ VH_NAME + " TEXT, " + VH_SCORE + " INTEGER);");

		}

		@Override
		public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub
			arg0.execSQL("DROP TABLE IF EXISTS " + DATABASE_EASY_TABLE);
			arg0.execSQL("DROP TABLE IF EXISTS " + DATABASE_MODERATE_TABLE);
			arg0.execSQL("DROP TABLE IF EXISTS " + DATABASE_HARD_TABLE);
			arg0.execSQL("DROP TABLE IF EXISTS " + DATABASE_VERYHARD_TABLE);
			onCreate(arg0);

		}
	}

	public TableDB(Context c) {
		ourContext = c;
	}

	public TableDB open() {
		OurHelper = new DbHelper(ourContext);
		ourDatabase = OurHelper.getWritableDatabase();
		return this;

	}

	public void close() {
		OurHelper.close();
	}

	// for easy table
	public String getEasyNameResult() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { E_NAME };
		Cursor c = ourDatabase.query(DATABASE_EASY_TABLE, columns, null, null,
				null, null, E_SCORE + " DESC", "5");
		String result = "NAME\n";

		int iName = c.getColumnIndex(E_NAME);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(iName) + "\n";
		}
		return result;
	}

	public String getEasyScoreResult() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { E_SCORE };
		Cursor c = ourDatabase.query(DATABASE_EASY_TABLE, columns, null, null,
				null, null, E_SCORE + " DESC", "5");
		String result = "SCORE\n";

		int iScore = c.getColumnIndex(E_SCORE);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(iScore) + "\n";
		}
		return result;
	}

	public int getNumOfEasyRows() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { E_NAME, E_SCORE };
		Cursor c = ourDatabase.query(DATABASE_EASY_TABLE, columns, null, null,
				null, null, E_SCORE + " DESC");
		int i = 0;
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			i++;
		}
		return i;
	}

	public String getFifthEasyScore() {
		// TODO Auto-generated method stub
		if (getNumOfEasyRows() >= 5) {
			String[] columns = new String[] { E_NAME, E_SCORE };
			Cursor c = ourDatabase.query(DATABASE_EASY_TABLE, columns, null,
					null, null, null, E_SCORE + " DESC", "5");
			int iScore = c.getColumnIndex(E_SCORE);

			c.moveToLast();
			return c.getString(iScore);
		} else
			return "0";
	}

	public long insertEasyTable(String myName, String getScore) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(E_NAME, myName);
		cv.put(E_SCORE, getScore);

		return ourDatabase.insert(DATABASE_EASY_TABLE, null, cv);

	}

	// for moderate table
	public String getModerateNameResult() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { M_NAME };
		Cursor c = ourDatabase.query(DATABASE_MODERATE_TABLE, columns, null,
				null, null, null, M_SCORE + " DESC", "5");
		String result = "NAME\n";

		int iName = c.getColumnIndex(M_NAME);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(iName) + "\n";
		}
		return result;
	}

	public String getModerateScoreResult() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { M_SCORE };
		Cursor c = ourDatabase.query(DATABASE_MODERATE_TABLE, columns, null,
				null, null, null, M_SCORE + " DESC", "5");
		String result = "SCORE\n";

		int iScore = c.getColumnIndex(M_SCORE);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(iScore) + "\n";
		}
		return result;
	}

	public int getNumOfModerateRows() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { M_NAME, M_SCORE };
		Cursor c = ourDatabase.query(DATABASE_MODERATE_TABLE, columns, null,
				null, null, null, M_SCORE + " DESC");
		int i = 0;
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			i++;
		}
		return i;
	}

	public String getFifthModerateScore() {
		// TODO Auto-generated method stub
		if (getNumOfModerateRows() >= 5) {
			String[] columns = new String[] { M_NAME, M_SCORE };
			Cursor c = ourDatabase.query(DATABASE_MODERATE_TABLE, columns,
					null, null, null, null, M_SCORE + " DESC", "5");
			int iScore = c.getColumnIndex(M_SCORE);

			c.moveToLast();
			return c.getString(iScore);
		} else
			return "0";
	}

	public long insertModerateTable(String myName, String getScore) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(M_NAME, myName);
		cv.put(M_SCORE, getScore);

		return ourDatabase.insert(DATABASE_MODERATE_TABLE, null, cv);

	}

	// for hard table
	public String getHardNameResult() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { H_NAME };
		Cursor c = ourDatabase.query(DATABASE_HARD_TABLE, columns, null, null,
				null, null, H_SCORE + " DESC", "5");
		String result = "NAME\n";

		int iName = c.getColumnIndex(H_NAME);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(iName) + "\n";
		}
		return result;
	}

	public String getHardScoreResult() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { H_SCORE };
		Cursor c = ourDatabase.query(DATABASE_HARD_TABLE, columns, null, null,
				null, null, H_SCORE + " DESC", "5");
		String result = "SCORE\n";

		int iScore = c.getColumnIndex(H_SCORE);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(iScore) + "\n";
		}
		return result;
	}

	public int getNumOfHardRows() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { H_NAME, H_SCORE };
		Cursor c = ourDatabase.query(DATABASE_HARD_TABLE, columns, null, null,
				null, null, H_SCORE + " DESC");
		int i = 0;
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			i++;
		}
		return i;
	}

	public String getFifthHardScore() {
		// TODO Auto-generated method stub
		if (getNumOfHardRows() >= 5) {
			String[] columns = new String[] { H_NAME, H_SCORE };
			Cursor c = ourDatabase.query(DATABASE_HARD_TABLE, columns, null,
					null, null, null, H_SCORE + " DESC", "5");
			int iScore = c.getColumnIndex(H_SCORE);

			c.moveToLast();
			return c.getString(iScore);
		} else
			return "0";
	}

	public long insertHardTable(String myName, String getScore) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(H_NAME, myName);
		cv.put(H_SCORE, getScore);

		return ourDatabase.insert(DATABASE_HARD_TABLE, null, cv);

	}

	// for very hard table
	public String getVeryHardNameResult() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { VH_NAME };
		Cursor c = ourDatabase.query(DATABASE_VERYHARD_TABLE, columns, null,
				null, null, null, VH_SCORE + " DESC", "5");
		String result = "NAME\n";

		int iName = c.getColumnIndex(VH_NAME);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(iName) + "\n";
		}
		return result;
	}

	public String getVeryHardScoreResult() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { VH_SCORE };
		Cursor c = ourDatabase.query(DATABASE_VERYHARD_TABLE, columns, null,
				null, null, null, VH_SCORE + " DESC", "5");
		String result = "SCORE\n";

		int iScore = c.getColumnIndex(VH_SCORE);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(iScore) + "\n";
		}
		return result;
	}

	public int getNumOfVeryHardRows() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { VH_NAME, VH_SCORE };
		Cursor c = ourDatabase.query(DATABASE_VERYHARD_TABLE, columns, null,
				null, null, null, VH_SCORE + " DESC");
		int i = 0;
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			i++;
		}
		return i;
	}

	public String getFifthVeryHardScore() {
		// TODO Auto-generated method stub
		if (getNumOfVeryHardRows() >= 5) {
			String[] columns = new String[] { VH_NAME, VH_SCORE };
			Cursor c = ourDatabase.query(DATABASE_VERYHARD_TABLE, columns,
					null, null, null, null, VH_SCORE + " DESC", "5");
			int iScore = c.getColumnIndex(VH_SCORE);

			c.moveToLast();
			return c.getString(iScore);
		} else
			return "0";
	}

	public long insertVeryHardTable(String myName, String getScore) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(VH_NAME, myName);
		cv.put(VH_SCORE, getScore);

		return ourDatabase.insert(DATABASE_VERYHARD_TABLE, null, cv);

	}
}