package com.ammad.anwer;

import java.util.ArrayList;
import java.util.Queue;
import java.util.Random;

import com.tapfortap.AdView;
import com.tapfortap.TapForTap;

import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Memory extends Activity implements View.OnClickListener {

	Button bt0;
	Button bt1;
	Button bt2;
	Button bt3;
	Button bt4;
	Button bt5;
	Button bt6;
	Button bt7;
	Button bt8;
	Button bt9;
	Button btst;
	TextView TvScore;
	TextView TvHint;
	int score;
	Integer Cindex; // variable to keep the index of button to be pressed by the
					// app
	Integer turn; // tells whether it is turn of user or of app
	Integer Pindex; // keep the record of number of buttons pressed by the user
	ArrayList<Integer> keep;
	int check = 0;
	boolean awein = false;
	Runnable mUpdateResults;
	Integer integer;
	Intent GameOver;
	Integer GameLevel = 0;
	SoundPool sp1, sp2;
	int sound1 = 0;
	int sound2 = 0;
	boolean isSound = true;
	WakeLock wL;
	Queue<Integer> myqueue;
	boolean isLock = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		PowerManager pM = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wL = pM.newWakeLock(PowerManager.FULL_WAKE_LOCK, "whatever");
		super.onCreate(savedInstanceState);
		wL.acquire();
		 TapForTap.setDefaultAppId("562f39e0-c854-012f-fb8c-4040d804a637");
         TapForTap.checkIn(this);
		setContentView(R.layout.activity_memory);
		 // Now get the AdView and load TapForTap ads!
        AdView adView = (AdView) findViewById(R.id.ad_view);
        adView.loadAds();
		initialize();
		showHelpDialogbox();

	}

	private void showHelpDialogbox() {
		// TODO Auto-generated method stub
		final AlertDialog AD = new AlertDialog.Builder(Memory.this).create();
		if (GameLevel == 1) {
			AD.setTitle("How To Play Easy Level");
			AD.setMessage("Repeat the sequence of buttons pressed and test your memory span as the sequence grows!\n Good Luck!!!");
		} else if (GameLevel == 2) {
			AD.setTitle("How To Play Moderate Level");
			AD.setMessage("You are Only shown the next in the sequence number. Repeat the whole sequence and test your memory span as the sequence grows!\nGood Luck!!!");
		} else if (GameLevel == 3) {
			AD.setTitle("How To Play Hard Level");
			AD.setMessage("A new sequence is generated after every turn. Repeat the sequence and test your memory span as the sequence grows!\nGood Luck!!!");
		} else if (GameLevel == 4) {
			AD.setTitle("How To Play Very Hard Level");
			AD.setMessage("A new sequence is generated after every turn. Repeat the sequence IN REVERSE ORDER and test your memory span as the sequence grows!\nGood Luck!!!");
		}
		AD.setButton("OK", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				AD.dismiss();
			}
		});
		AD.show();

	}

	private void doEverything() {
		// TODO Auto-generated method stub

		Cindex = 0;
		if (GameLevel != 3 && GameLevel != 4) {

			Random crazy = new Random();
			keep.add(crazy.nextInt(10));
			whichToClick();

		} else {
			int length = keep.size();
			keep.clear();
			for (int i = 0; i <= length; i++) {
				Random crazy = new Random();
				keep.add(crazy.nextInt(10));
			}
			whichToClick();
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		wL.release();
		finish();
	}

	private void showButtonPressing() {
		if (isSound)
			sp1.play(sound1, 1, 1, 0, 0, 1);
		switch (integer) {
		case 0:
			bt0.post(new Runnable() {
				public void run() {
					bt0.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt0.post(new Runnable() {
				public void run() {
					bt0.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 1:
			bt1.post(new Runnable() {
				public void run() {
					bt1.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt1.post(new Runnable() {
				public void run() {
					bt1.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 2:
			bt2.post(new Runnable() {
				public void run() {
					bt2.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt2.post(new Runnable() {
				public void run() {
					bt2.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 3:
			bt3.post(new Runnable() {
				public void run() {
					bt3.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt3.post(new Runnable() {
				public void run() {
					bt3.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 4:
			bt4.post(new Runnable() {
				public void run() {
					bt4.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt4.post(new Runnable() {
				public void run() {
					bt4.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 5:
			bt5.post(new Runnable() {
				public void run() {
					bt5.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt5.post(new Runnable() {
				public void run() {
					bt5.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 6:
			bt6.post(new Runnable() {
				public void run() {
					bt6.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt6.post(new Runnable() {
				public void run() {
					bt6.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 7:
			bt7.post(new Runnable() {
				public void run() {
					bt7.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt7.post(new Runnable() {
				public void run() {
					bt7.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 8:
			bt8.post(new Runnable() {
				public void run() {
					bt8.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt8.post(new Runnable() {
				public void run() {
					bt8.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 9:
			bt9.post(new Runnable() {
				public void run() {
					bt9.setPressed(true);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bt9.post(new Runnable() {
				public void run() {
					bt9.setPressed(false);
				}
			});
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	private void whichToClick() {

		new Thread() {
			public void run() {
				// this is automatic clicking
				try {
					Thread.sleep(750);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (GameLevel == 2) // for moderate
				{
					integer = keep.get(keep.size() - 1);
					showButtonPressing();
				} else {
					disableAll();
					while (Cindex < keep.size()) {
						integer = keep.get(Cindex);
						Cindex++;
						if (Cindex == keep.size()) {
							new Thread() {
								public void run() {
									bt0.setClickable(true);
									bt1.setClickable(true);
									bt2.setClickable(true);
									bt3.setClickable(true);
									bt4.setClickable(true);
									bt5.setClickable(true);
									bt6.setClickable(true);
									bt7.setClickable(true);
									bt8.setClickable(true);
									bt9.setClickable(true);
								}
							}.start();
						}
						showButtonPressing();
					}

				}
				Cindex = 0;
				if (GameLevel != 4)
					Pindex = 0;
				else
					Pindex = keep.size() - 1;
				turn = 2;

			}

			private void disableAll() {
				// TODO Auto-generated method stub
				bt0.setClickable(false);
				bt1.setClickable(false);
				bt2.setClickable(false);
				bt3.setClickable(false);
				bt4.setClickable(false);
				bt5.setClickable(false);
				bt6.setClickable(false);
				bt7.setClickable(false);
				bt8.setClickable(false);
				bt9.setClickable(false);
			}
		}.start();

	}

	private void initialize() {
		// TODO Auto-generated method stub
		// /initializing views
		bt0 = (Button) findViewById(R.id.b0);
		bt1 = (Button) findViewById(R.id.b1);
		bt2 = (Button) findViewById(R.id.b2);
		bt3 = (Button) findViewById(R.id.b3);
		bt4 = (Button) findViewById(R.id.b4);
		bt5 = (Button) findViewById(R.id.b5);
		bt6 = (Button) findViewById(R.id.b6);
		bt7 = (Button) findViewById(R.id.b7);
		bt8 = (Button) findViewById(R.id.b8);
		bt9 = (Button) findViewById(R.id.b9);
		btst = (Button) findViewById(R.id.St);
		TvScore = (TextView) findViewById(R.id.Score);

		bt0.setOnClickListener(this);
		bt1.setOnClickListener(this);
		bt2.setOnClickListener(this);
		bt3.setOnClickListener(this);
		bt4.setOnClickListener(this);
		bt5.setOnClickListener(this);
		bt6.setOnClickListener(this);
		bt7.setOnClickListener(this);
		bt8.setOnClickListener(this);
		bt9.setOnClickListener(this);
		btst.setOnClickListener(this);
		btst.getBackground().setAlpha(0);

		// disabling All
		bt0.setClickable(false);
		bt1.setClickable(false);
		bt2.setClickable(false);
		bt3.setClickable(false);
		bt4.setClickable(false);
		bt5.setClickable(false);
		bt6.setClickable(false);
		bt7.setClickable(false);
		bt8.setClickable(false);
		bt9.setClickable(false);

		// initializing variables
		keep = new ArrayList<Integer>();
		score = 0;
		Cindex = 0;
		turn = 1;
		Pindex = 0;

		Bundle gotBasket = getIntent().getExtras();
		String getLevel = gotBasket.getString("key");
		GameLevel = Integer.parseInt(getLevel);
		sp1 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		sp2 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		sound1 = sp1.load(this, R.raw.buttontap, 1);
		sound2 = sp2.load(this, R.raw.wrong, 1);

	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.St) {

			// enabling All
			bt0.setClickable(true);
			bt1.setClickable(true);
			bt2.setClickable(true);
			bt3.setClickable(true);
			bt4.setClickable(true);
			bt5.setClickable(true);
			bt6.setClickable(true);
			bt7.setClickable(true);
			bt8.setClickable(true);
			bt9.setClickable(true);
			new Thread() {
				public void run() {

					btst.post(new Runnable() {
						public void run() {
							btst.setVisibility(View.GONE);
						}
					});

				}
			}.start();
			doEverything();
		} else if (v.getId() == R.id.b0) {

			if (turn == 2) {

				if (keep.get(Pindex) == 0) {
					checkAndEveryThing();

				} else {
					endTheGame();

				}

			}
		} else if (v.getId() == R.id.b1) {
			if (turn == 2) {

				if (keep.get(Pindex) == 1) {
					checkAndEveryThing();

				} else {
					endTheGame();

				}

			}

		} else if (v.getId() == R.id.b2) {
			if (turn == 2) {

				// isLock = true;
				if (keep.get(Pindex) == 2) {
					checkAndEveryThing();

				} else {
					endTheGame();
				}

			}

		} else if (v.getId() == R.id.b3) {
			if (turn == 2) {

				if (keep.get(Pindex) == 3) {
					checkAndEveryThing();

				} else {
					endTheGame();

				}

			}

		} else if (v.getId() == R.id.b4) {
			if (turn == 2) {

				if (keep.get(Pindex) == 4) {
					checkAndEveryThing();

				} else {
					endTheGame();

				}

			}

		} else if (v.getId() == R.id.b5) {
			if (turn == 2) {

				if (keep.get(Pindex) == 5) {
					checkAndEveryThing();

				} else {
					endTheGame();

				}
			}
		} else if (v.getId() == R.id.b6) {
			if (turn == 2) {

				if (keep.get(Pindex) == 6) {
					checkAndEveryThing();

				} else {
					endTheGame();

				}
			}
		} else if (v.getId() == R.id.b7) {
			if (turn == 2) {
				if (keep.get(Pindex) == 7) {
					checkAndEveryThing();

				} else {
					endTheGame();

				}
			}

		} else if (v.getId() == R.id.b8) {
			if (turn == 2) {

				if (keep.get(Pindex) == 8) {
					checkAndEveryThing();
				} else {
					endTheGame();

				}

			}
		} else if (v.getId() == R.id.b9) {
			if (turn == 2) {
				if (keep.get(Pindex) == 9) {
					checkAndEveryThing();
				} else {
					endTheGame();

				}
			}
		}

	}

	private void pressNext(int next) {
		// TODO Auto-generated method stub
		switch (next) {
		case 0:
			bt0.performClick();
			break;
		case 1:
			bt1.performClick();
			break;
		case 2:
			bt2.performClick();
			break;
		case 3:
			bt3.performClick();
			break;
		case 4:
			bt4.performClick();
			break;
		case 5:
			bt5.performClick();
			break;
		case 6:
			bt6.performClick();
			break;
		case 7:
			bt7.performClick();
			break;
		case 8:
			bt8.performClick();
			break;
		case 9:
			bt9.performClick();
			break;
		}
	}

	private void checkAndEveryThing() {
		// TODO Auto-generated method stub
		if (isSound)
			sp1.play(sound1, 1, 1, 0, 0, 1);

		if (GameLevel != 4) {
			Pindex++;
			if (Pindex == keep.size()) {
				score++;
				TvScore.setText("Span: " + Integer.toString(score));
				turn = 1;
				// myqueue.clear();
				doEverything();
			}
		} else {
			Pindex--;
			if (Pindex == -1) {
				score++;
				TvScore.setText("Span: " + Integer.toString(score));
				turn = 1;
				// isLock = false;
				doEverything();
			}

		}
	}

	private void endTheGame() {
		// TODO Auto-generated method stub
		if (isSound)
			sp2.play(sound2, 1, 1, 0, 0, 1);
		wL.release();
		String myScore = String.valueOf(score);
		Intent intent = new Intent(Memory.this, EndGame.class);
		intent.putExtra("key", myScore);
		intent.putExtra("level", GameLevel.toString());
		finish();
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu);
		MenuInflater blowUp = getMenuInflater();
		blowUp.inflate(R.menu.my_menu, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.itRestart:
			keep.clear();
			score = 0;
			TvScore.setText("Span: 0");
			doEverything();
			break;
		case R.id.itHelp:
			showHelpDialogbox();
			break;
		case R.id.itSound:
			isSound = !isSound;
			break;
		}
		return false;
	}

}
