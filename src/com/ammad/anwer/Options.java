package com.ammad.anwer;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Options extends Activity implements OnClickListener {

	Button easy;
	Button moderate;
	Button hard;
	Button vhard;
	TextView Intro;
	Bundle basket = new Bundle();
	Intent intent;
	SoundPool sp;
	int sound = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.options);
		initialize();
	}

	private void initialize() {
		// TODO Auto-generated method stub
		Intro = (TextView) findViewById(R.id.tvIntro);
		easy = (Button) findViewById(R.id.bEasy);
		moderate = (Button) findViewById(R.id.bModerate);
		hard = (Button) findViewById(R.id.bHard);
		vhard = (Button) findViewById(R.id.bVeryHard);

		easy.setOnClickListener(this);
		moderate.setOnClickListener(this);
		hard.setOnClickListener(this);
		vhard.setOnClickListener(this);

		easy.getBackground().setAlpha(100);
		moderate.getBackground().setAlpha(100);
		hard.getBackground().setAlpha(100);
		vhard.getBackground().setAlpha(100);
		sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		sound = sp.load(this, R.raw.click, 1);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (sound != 0)
			sp.play(sound, 1, 1, 0, 0, 1);
		switch (v.getId()) {
		case R.id.bEasy:

			basket.putString("key", "1");
			break;
		case R.id.bModerate:
			basket.putString("key", "2");
			break;
		case R.id.bHard:
			basket.putString("key", "3");
			break;
		case R.id.bVeryHard:
			basket.putString("key", "4");
			break;
		}
		intent = new Intent(Options.this, Memory.class);
		intent.putExtras(basket);
		startActivity(intent);

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// finish();
	}

}
